package com.dqteam.disys.disys;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class DiagnosticarFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.diagnosticar_view, container, false);

        //TextView tv = (TextView) v.findViewById(R.id.tv_Diagnosticar);
        //tv.setText(getArguments().getString("msg"));

        return v;
    }

    public static DiagnosticarFragment newInstance(String text) {
        DiagnosticarFragment f = new DiagnosticarFragment();
        Bundle b = new Bundle();
        b.putString("msg", text);

        f.setArguments(b);

        return f;
    }
}
