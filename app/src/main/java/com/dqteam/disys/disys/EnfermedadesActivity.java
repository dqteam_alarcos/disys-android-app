package com.dqteam.disys.disys;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;

public class EnfermedadesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enfermedades);

        Intent intent = getIntent();
        String entrada = intent.getStringExtra("ENFERMEDADES").trim();
        entrada += ";";

        ArrayList<Integer> aux = new ArrayList<Integer>();
        ArrayList<BarEntry> enfermedades = new ArrayList<>();
        ArrayList<String> etiquetas = new ArrayList<String>();

        String[] enf = entrada.split(";");

        for (int i = 0; i < enf.length; i++) {
            try{
                aux.add(Integer.parseInt(enf[i]));
            } catch (Exception e) {
                etiquetas.add(enf[i]);
            }
        }

        for (int i = 0; i < aux.size(); i++){
            enfermedades.add(new BarEntry(aux.get(i), i));
        }

        BarDataSet dataset = new BarDataSet(enfermedades, "% de probabilidad");

        BarChart grafica = new BarChart(getApplicationContext());
        setContentView(grafica);

        BarData datos = new BarData(etiquetas, dataset);
        grafica.setData(datos);
    }
}
