package com.dqteam.disys.disys;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private List<String> listaSintomas = new ArrayList<String>();
    private List<   EditText> listEdits = new ArrayList<EditText>();
    private String sintomasAEnviar = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch(pos) {
                case 0: return InicioFragment.newInstance("Página de inicio");
                case 1: return DiagnosticarFragment.newInstance("Página para diagnosticar");
                case 2: return MiCiudadFragment.newInstance("Página para mostrar las enferemedades más comunes de mi ciudad");
                case 3: return MapaFragment.newInstance("Página para mostrar un mapa con las enfermedades más comunes.");
                default: return InicioFragment.newInstance("Página de inicio");
            }
        }

        @Override
        public int getCount() {
            // Show 4 total pages.
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Inicio";
                case 1:
                    return "Diagnosticar";
                case 2:
                    return "Mi ciudad";
                case 3:
                    return "Mapa";
            }
            return null;
        }
    }

    private class CargarEnfermedadTask extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... params) {

            //String urlString = "http://192.168.1.106:8080/botanic/enfermedades";
            String urlString = "172.19.184.113:8080/botanic/enfermedades";
            String resultToDisplay = "";
            DataOutputStream printout;
            try {
                URL url = new URL(urlString);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                urlConnection.setUseCaches(false);
                urlConnection.setRequestMethod("POST");

                // Send POST output.
                printout = new DataOutputStream(urlConnection.getOutputStream());
                printout.writeBytes(params[0]);
                printout.flush();
                printout.close();
                urlConnection.connect();

                InputStream is = urlConnection.getInputStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                String line;
                StringBuffer response = new StringBuffer();
                while((line = rd.readLine()) != null) {
                    response.append(line);
                    response.append('\r');
                }
                rd.close();
                resultToDisplay = response.toString();
            } catch (Exception e ) {
                System.out.println(e.getMessage());
                return e.getMessage();
            }

            return resultToDisplay;
        }

        protected void onPostExecute(String response) {
            //Toast.makeText(MainActivity.this, "onPostExecute " + response, Toast.LENGTH_LONG).show();
            //Intent i = new Intent(MainActivity.this, MostrarEnfermedadesFragment.class);
            //i.putExtra("ENFERMEDADES", response);
            Intent i = new Intent(MainActivity.this, EnfermedadesActivity.class);
            i.putExtra("ENFERMEDADES", response);
            startActivity(i);
        }
    }

    // EVENTS //

    public void DiagnosticarOnClick(View v){
        //Toast.makeText(MainActivity.this, "Clickeado !", Toast.LENGTH_LONG).show();
        for (int i = 0; i < listEdits.size(); i++){
            sintomasAEnviar += listEdits.get(i).getText().toString() + ";";
        }

        //String url = String.format("http://mymovieapi.com/?title=%1$s&type=json&limit=10", titulo);
        new CargarEnfermedadTask().execute(sintomasAEnviar);
    }

    public void NuevoSintomaClick(View v) {
        //Toast.makeText(MainActivity.this, "Añadir nuevo síntoma !", Toast.LENGTH_LONG).show();
        TableRow synthoms = (TableRow) findViewById(R.id.tableRow);
        TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,TableRow.LayoutParams.WRAP_CONTENT, 1.0f);
        EditText et = new EditText(this);
        et.setLayoutParams(params);
        et.setSingleLine(true);
        et.setLines(1);
        listEdits.add(et);
        synthoms.addView(et);
    }
}
