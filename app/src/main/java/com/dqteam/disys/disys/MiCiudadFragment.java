package com.dqteam.disys.disys;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;

import java.util.ArrayList;

/**
 * Created by Andrés on 26/02/2016.
 */
public class MiCiudadFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.miciudad_view, container, false);


        String entrada = "gripe;68;gastroenteritis;21;otitis;11;";

        ArrayList<Integer> aux = new ArrayList<Integer>();
        ArrayList<BarEntry> enfermedades = new ArrayList<>();
        ArrayList<String> etiquetas = new ArrayList<String>();

        String[] enf = entrada.split(";");

        for (int i = 0; i < enf.length; i++) {
            try{
                aux.add(Integer.parseInt(enf[i]));
            } catch (Exception e) {
                etiquetas.add(enf[i]);
            }
        }

        for (int i = 0; i < aux.size(); i++){
            enfermedades.add(new BarEntry(aux.get(i), i));
        }


        return v;
    }

    public static MiCiudadFragment newInstance(String text) {

        MiCiudadFragment f = new MiCiudadFragment();
        Bundle b = new Bundle();
        b.putString("msg", text);

        f.setArguments(b);

        return f;
    }

}
